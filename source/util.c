#include "global.h"


//void init_queue(QueueType *q);
static int is_empty(const QueueType *q);
static int is_full(const QueueType *q);
//QueueStatus enqueue(QueueType *q, unsigned char str);
static QueueStatus dequeue(QueueType *q,unsigned char *str);
//ErrorStatus usart1_getchar(char *data);

void SendChar(USART_TypeDef* USARTx, char chmsg);
//void SendString(USART_TypeDef* USARTx, char* pstrmsg);
void WriteBYTE(USART_TypeDef* USARTx, const char* pstrmsg, uint16_t uisize);

//void delay_ms(u16 cnt);
//void delay_us(u32 cnt);

//uint8_t value_parsing(uint8_t cnt,char * rxbuf);
//uint16_t lvalue_parsing(uint8_t cnt,char * rxbuf);
//float fvalue_parsing(uint8_t cnt, const char * rxbuf);
//int StringFind(char* pstrsrc, char* pstrset);


void init_queue(QueueType *q)
{
  
  q->front = 0; 
  q->rear = 0;
}

static int is_empty(const QueueType *q)
{
  return(q->front == q->rear);
}

static int is_full(const QueueType *q)
{
  return(((q->rear+1u)%MAX_QUEUE_SIZE) == q->front);
}

QueueStatus enqueue(QueueType *q, unsigned char str)
{
  QueueStatus return_result = QUEUE_SUCCESS;
  
  if(is_full(q))  { return_result = QUEUE_ERROR; } 
  
  q->rear++;
  if(q->rear==MAX_QUEUE_SIZE)  { q->rear=0; } 
  q->queue[q->rear]=str;  
  
  return return_result;
}

static QueueStatus dequeue(QueueType *q,unsigned char *str)
{
  QueueStatus return_result = QUEUE_SUCCESS;
  
  if(is_empty(q))  { return_result = QUEUE_ERROR; } 
  
  q->front++;
  if(q->front==MAX_QUEUE_SIZE)  { q->front=0; } 
  *str = q->queue[q->front];
  
  return return_result;
}

ErrorStatus usart1_getchar(char *data)
{
    ErrorStatus return_result;
      
    if(dequeue(&USART1_Queue,(unsigned char *)data)==QUEUE_SUCCESS)  {return_result = SUCCESS;} 
    else  {return_result =  ERROR; } 
    
    return return_result;
}
ErrorStatus usart2_getchar(char *data)
{
    ErrorStatus return_result;
    
    if(dequeue(&USART2_Queue,(unsigned char *)data)==QUEUE_SUCCESS)  {return_result = SUCCESS;} 
    else  {return_result = ERROR;} 
    
    return return_result;
}
//-----------------------------------------------------------------------------------------------//
void SendChar(USART_TypeDef* USARTx, char chmsg)
{
  while(!(USARTx->SR & USART_FLAG_TXE)){}
  	USARTx->DR = (chmsg & 0x01FF);
}
//-----------------------------------------------------------------------------------------------//
void SendString(USART_TypeDef* USARTx, const char* pstrmsg)
{
	//char c;
        
        while(*pstrmsg!=0)
	{
          while(!(USARTx->SR & USART_FLAG_TXE)){}
  		USARTx->DR = (*pstrmsg & 0x01FF);
                pstrmsg++;
	}
}
//-----------------------------------------------------------------------------------------------//
void WriteBYTE(USART_TypeDef* USARTx, const char* pstrmsg, uint16_t uisize)
{
	uint16_t i;
               
	for(i=0;i<uisize;i++)
	{
          while(!(USARTx->SR & USART_FLAG_TXE)){}
  		USARTx->DR = (*pstrmsg & 0x01FF);
		pstrmsg++;
	}
}

uint8_t Make_CheckSum(const char *Data_packet, int packet_size)
{
    uint8_t CheckSum=0;
    
    for(int i=0; i<packet_size; i++) 
    {
      if(Data_packet[i] == '$') { continue; }
      if(Data_packet[i] == '*') { break; }
        
      CheckSum^=Data_packet[i];
    }
    
    return CheckSum;
}

/*******************************************************************************
* Function Name : void UART1_PutStr(char *pStr)
* Description   : UART1 믄자열 출력
* Parameters    : 문자열
* Return        : 1byte
*******************************************************************************/
static void UART1_PutChar(uint8_t data)                         
{
  USART_SendData(USART1, data);
  while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET){}     
}
void USART1_PutStr(const char *pStr)
{
  while(*pStr != '\0') { UART1_PutChar(*(pStr++)); }
}

static void USART2_PutChar(uint8_t data)                         
{
  USART_SendData(USART2, data);
  while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET){}     
}
void USART2_PutStr(const char *pStr)
{
  while(*pStr != '\0') { USART2_PutChar(*(pStr++)) ;}
}

/////////////////////////////////////////////////////////////////////////
//  delay_util

void delay_ms(u16 cnt)
{
  u16 i,x;
  if(cnt<65534u)
  {
    for(i=0u;i<5000u;i++)
    {
     for(x=0u;x<cnt;x++)
      {
      
      }
    }
  }
}

void delay_us(uint32_t cnt)
{
  uint16_t i;
  uint32_t x;
  
  if(cnt<65534u)
  {
      for(i=0u;i<8u;i++)
      {
        for(x=0u;x<cnt;x++)
        {
          
        }
      }
  }
}

/*
uint8_t value_parsing(uint8_t cnt, const char * rxbuf)
{
	uint8_t i,x;
        
	i=cnt;
        
	char stemp[10];
	
	for(x=0;x<5;x++)
	{
            i++;
            
		if(rxbuf[i]==0)
		{
			stemp[x]=0;
			break;
		}
		else { stemp[x]=rxbuf[i]; }	
	}
	
	stemp[3]=0;	

	return atoi(stemp);
}
*/