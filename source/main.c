/**
  ******************************************************************************
  * @file    SDIO/uSDCard/main.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <string.h>

void main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */

  /* Initialize LEDs available on STM3210X-EVAL board *************************/

  /* Interrupt Config */
 // delay_ms(5);  fads
  SystemInit();
  RCC_Configuration();
  GPIO_Configuration();
  UART_Configuration();
  TIM_Configuration();
  ADC_Configuration();
  
  SystemTickConfiguration();
  
  init_queue(&USART1_Queue);  
  init_queue(&USART2_Queue);  
  
  NVIC_Configuration();
  
  SysTick_CounterCmd(SysTick_Counter_Clear);
  SysTick_CounterCmd(SysTick_Counter_Enable);  
    
  init_value();
    
   
  while(1)
  {   

     temp_chk_P3_3VIN = readADC1_ch8(ADC_Channel_8);                //ADC_P3_3VIN
     temp_chk_P3_3VDCD = readADC1_ch9(ADC_Channel_9);               //ADC_P3_3VDCD
     temp_chk_P3_3FPGA = readADC1_ch10(ADC_Channel_10);             //ADC_P3_3FPGA
     temp_chk_P3_3STM = readADC1_ch11(ADC_Channel_11);              //ADC_P3_3STM
     temp_chk_P5VIN = readADC1_ch12(ADC_Channel_12);                //ADC_P5VIN
     temp_chk_P12VIN = readADC1_ch13(ADC_Channel_13);               //ADC_P12VIN     
     temp_chk_M5VIN = readADC1_ch14(ADC_Channel_14);                //ADC_M5VIN
     temp_chk_M12VIN = readADC1_ch15(ADC_Channel_15);               //ADC_M12VIN 
     
     if(trig_event==1)
     {
       com_processing();
       trig_event=false;
     }
    
   }    
}       

static void init_value(void)
{

  rec_pwr_status.BITE_P3_3VIN = 0.0f;
  rec_pwr_status.BITE_P3_3VDCD = 0.0f;
  rec_pwr_status.BITE_P3_3FPGA = 0.0f;
  rec_pwr_status.BITE_P3_3STM = 0.0f;
  rec_pwr_status.BITE_P5VIN = 0.0f;
  rec_pwr_status.BITE_P12VIN = 0.0f;
  rec_pwr_status.BITE_M5VIN = 0.0f;
  rec_pwr_status.BITE_M12VIN = 0.0f;  
    
}

//////////////////////////////////////////////////multichannel ADC////////////////////////////////////////////////////////////

static uint16_t readADC1_ch8(u8 ADC1_Channel_8)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_8, 1, ADC_SampleTime_239Cycles5);  //ADC_P3_3VIN 
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch8 = ADC_GetConversionValue(ADC1);
  chk_P3_3VIN=(((float)ADC1ConValue_ch8*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch8 >= 4095u) {chk_P3_3VIN = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}
  
static uint16_t readADC1_ch9(u8 ADC1_Channel_9)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_9, 1, ADC_SampleTime_239Cycles5);  //ADC_P3_3VDCD
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch9 = ADC_GetConversionValue(ADC1);
  chk_P3_3VDCD=(((float)ADC1ConValue_ch9*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch9 >= 4095u) {chk_P3_3VDCD = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}

static uint16_t readADC1_ch10(u8 ADC1_Channel_10)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_10, 1, ADC_SampleTime_239Cycles5);  //ADC_P3_3FPGA
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch10 = ADC_GetConversionValue(ADC1);
  chk_P3_3FPGA=(((float)ADC1ConValue_ch10*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch10 >= 4095u) {chk_P3_3FPGA = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}
  
static uint16_t readADC1_ch11(u8 ADC1_Channel_11)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_11, 1, ADC_SampleTime_239Cycles5);  //ADC_P3_3STM
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch11 = ADC_GetConversionValue(ADC1);
  chk_P3_3STM=(((float)ADC1ConValue_ch11*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch11 >= 4095u) {chk_P3_3STM = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}

static uint16_t readADC1_ch12(u8 ADC1_Channel_12)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_12, 1, ADC_SampleTime_239Cycles5);  //ADC_P5VIN
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch12 = ADC_GetConversionValue(ADC1);
  chk_P5VIN=(((float)ADC1ConValue_ch12*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch12 >= 4095u) {chk_P5VIN = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}
  
static uint16_t readADC1_ch13(u8 ADC1_Channel_13)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_13, 1, ADC_SampleTime_239Cycles5);  //ADC_P12VIN 
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch13 = ADC_GetConversionValue(ADC1);
  chk_P12VIN=(((float)ADC1ConValue_ch13*0.00080586f)*6.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch13 >= 4095u) {chk_P12VIN = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}

static uint16_t readADC1_ch14(u8 ADC1_Channel_14)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_14, 1, ADC_SampleTime_239Cycles5);  //ADC_M5VIN
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch14 = ADC_GetConversionValue(ADC1);
  chk_M5VIN=(((((float)ADC1ConValue_ch14*0.00080586f)*6.0f)+0.3f)*-1.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch14 >= 4095u) {chk_M5VIN = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
}   

static uint16_t readADC1_ch15(u8 ADC1_Channel_15)
{
   // ADC2 regular channels configuration 
  ADC_RegularChannelConfig(ADC1, ADC1_Channel_15, 1, ADC_SampleTime_239Cycles5);  //ADC_M12VIN
  //start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  //wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}
  //Get the conversion value
  ADC1ConValue_ch15 = ADC_GetConversionValue(ADC1);
  chk_M12VIN=(((((float)ADC1ConValue_ch15*0.00080586f)*6.0f)-0.3f)*-1.0f); //분배법칙, 0.00080586 = 3.3V / 4095   Reg1=((ADC_value*0.00080586)*10k)/((Vref-(ADC_value*0.00080586))
  if(ADC1ConValue_ch15 >= 4095u) {chk_M12VIN = 0.0f;}
  return ADC_GetConversionValue(ADC1); 
} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void delay(u16 cnt)     
{
  u16 i,x;
  
  for(i=0u;i<5000u;i++){
    for(x=0u;x<cnt;x++)
    {
    }
  }
}


void TIM_Start(TIM_TypeDef* TIMx)
{
  TIMx->CNT=0;
  TIM_ITConfig(TIMx, TIM_IT_Update, ENABLE );
  TIM_Cmd(TIMx, ENABLE);
}

void TIM_Stop(TIM_TypeDef* TIMx)
{
  TIM_Cmd(TIMx, DISABLE);  
  TIMx->CNT=0;
  TIM_ITConfig(TIMx, TIM_IT_Update, DISABLE ); 
}
  
static void com_processing(void)
{ 
  char chksum;
  char strtemp[255];
  char BITE_Temp[20];
  
  rec_pwr_status.BITE_P3_3VIN=chk_P3_3VIN;
  rec_pwr_status.BITE_P3_3VDCD=chk_P3_3VDCD;
  rec_pwr_status.BITE_P3_3FPGA=chk_P3_3FPGA;
  rec_pwr_status.BITE_P3_3STM=chk_P3_3STM;
  rec_pwr_status.BITE_P5VIN=chk_P5VIN;
  rec_pwr_status.BITE_P12VIN=chk_P12VIN;
  rec_pwr_status.BITE_M5VIN=chk_M5VIN;
  rec_pwr_status.BITE_M12VIN=chk_M12VIN;  
  
  int com_send_result = sprintf(strtemp,"$REC_BITE,%.1f,%.1f,%.1f,%.1f,%.1f,*",chk_P5VIN,chk_M5VIN,chk_P3_3VDCD,chk_P3_3STM,chk_P3_3FPGA );
  chksum=Make_CheckSum(strtemp,sizeof(strtemp));
  com_send_result = sprintf(BITE_Temp,"%s%02X\r\n",strtemp, chksum);
  USART1_PutStr(BITE_Temp);
  
}    

//////////////////////////////////////////////////USART /////////////////////////////////////////////////////////////


void USART1_IRQHandler(void) //USART1 RS232
{
  uint16_t rxd;

  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
    rxd=USART_ReceiveData(USART1);
    QueueStatus Queuestatus_result = enqueue(&USART1_Queue,rxd);
  }
  else if(USART_GetITStatus(USART1, USART_IT_ORE_RX) != RESET)
  {
    //temp_ore=USART_ReceiveData(USART1);
    RxCounter1=0;
    USART_ClearITPendingBit(USART1,USART_IT_ORE_RX);
  }
  else
  {
  	/* exception */
  }
}

void Usart1_Parsing(void)
{

  while(usart1_getchar(&Usart1_RXD)==SUCCESS)
  {
    if(Usart1_RXD=='$')
    {
      usart1_headchk=1; 
      RxBuffer1[0]=Usart1_RXD;
      RxCounter1=1;
    }    
     if(usart1_headchk==1u)
     {
      if(Usart1_RXD=='\r') 
      {
        RxBuffer1[RxCounter1]=Usart1_RXD;
        RxBuffer1[RxCounter1]=0; 
        usart1_headchk=0;
      }
      else
      {  
         RxBuffer1[RxCounter1]=Usart1_RXD;
         if(RxCounter1<255u)
         {
           RxCounter1++;
         }
         else 
         {
           RxCounter1=0;
         }
      }    
     }
    else
    {
    	/* exception */
    }
  }     
}

void USART2_IRQHandler(void) //USART1 RS232
{
  uint16_t rxd;
  //char rxd;

  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
    rxd=USART_ReceiveData(USART2);
    QueueStatus Queuestatus_result = enqueue(&USART2_Queue,rxd);
  }
  else if(USART_GetITStatus(USART2, USART_IT_ORE_RX) != RESET)
  {
    //temp_ore=USART_ReceiveData(USART2);
    RxCounter2=0;
    USART_ClearITPendingBit(USART2,USART_IT_ORE_RX);
  }
  else
  {
  	/* exception */
  }
}

void Usart2_Parsing(void)
{
  while(usart2_getchar(&Usart2_RXD)==SUCCESS)
  {
    if(Usart2_RXD=='$')
    {
      usart2_headchk=1; 
      RxBuffer2[0]=Usart2_RXD;
      RxCounter2=1;
    }    
    if(usart2_headchk==1u)
    {
      if(Usart2_RXD=='\r') 
      {
        RxBuffer2[RxCounter2]=Usart2_RXD;
        RxBuffer2[RxCounter2+1u]=0; 
        usart2_headchk=0;
      }
      else
      {
         RxBuffer2[RxCounter2]=Usart2_RXD;
         if(RxCounter2<255u)  
         { 
           RxCounter2++; 
         } 
         else  
         {
           RxCounter2=0; 
         } 
      }    
    }
    else
    {
    	/* exception */
    }
  }     
}

////////////////////////////////////////////////Parsing

void SysTick_Handler(void)
{
  
  trig_event=true;   // 1sec
      
}
