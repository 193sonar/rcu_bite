#ifndef __STM32F10xINIT_H
#define __STM32F10xINIT_H

#include "global.h"

// SystemFunction
void SystemTickConfiguration(void);
void SYSCLKConfig_STOP(void);
void RCC_Configuration(void);
void NVIC_Configuration(void);
void GPIO_Configuration(void);
//void ExtInt_Configuration(void);
void UART_Configuration(void);
void RTC_Configuration(void);
void TIM_Configuration(void);
//void DAC_Configuration(void);
void ADC_Configuration(void);
//void I2C_Configuration(void);
void SPI_Configuration(void);
//void RTCALARM_Configuration(void);
//void PWM_Configuration(void);
//void Enable_STOPMODE_EXTI(void);
//void Disable_STOPMODE_EXTI(void);


#endif