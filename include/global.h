#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "integer.h"	/* Basic integer types */
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "math.h"
#include "limits.h"
#include "float.h"
#include "stdint.h"
#include "stm32f10x.h"

#define ADC1_DR_Address    ((u32)0x4001244C)

// PORTA
#define USART2_TX                   GPIO_Pin_2           //    peri
#define USART2_RX                   GPIO_Pin_3           //    peri
#define EP36_TP                     GPIO_Pin_4          //    input
#define EP37_TP                     GPIO_Pin_5          //    input
#define EP38_TP                     GPIO_Pin_6          //    input
#define EP39_TP                     GPIO_Pin_7          //    output
#define USART1_TX                   GPIO_Pin_9          //    peri
#define USART1_RX                   GPIO_Pin_10         //    peri
#define EP31_TP                     GPIO_Pin_15          //    output

// PORTB
#define ADC_P3_3VIN                 GPIO_Pin_0          //    input      
#define ADC_P3_3VDCD                GPIO_Pin_1          //    input
#define EP33_TP                     GPIO_Pin_3          //    output
#define EP32_TP                     GPIO_Pin_4          //    output

// PORTC
#define ADC_P3_3FPGA                 GPIO_Pin_0          //    input ADC
#define ADC_P3_3STM                  GPIO_Pin_1          //    input
#define ADC_P5VIN                    GPIO_Pin_2          //    output
#define ADC_P12VIN                   GPIO_Pin_3          //    output
#define ADC_M5VIN                    GPIO_Pin_4          //    output
#define ADC_M12VIN                   GPIO_Pin_5          //    output
//////////////////////////////////////////////////////////////////////////////////////

#define MAX_QUEUE_SIZE                1024u


//test pin 
#define EP31_TP_HIGH                          GPIOA->BRR = EP31_TP
#define EP31_TP_LOW                           GPIOA->BSRR = EP31_TP
#define EP31_TP_TOGGLE                        GPIOA->ODR ^= EP31_TP
///////////



/* Private typedef -----------------------------------------------------------*/


/* Select SPI : Chip Select pin low  */

typedef struct SQueue
{
   unsigned int front,rear;
   unsigned char queue[1024];
}QueueType; 

typedef union flvalue
{
  float fdata;
  long ldata;
}FL_value;

extern void delay_us(uint32_t cnt);
extern void delay_ms(u16 cnt);
extern void delay(u16 cnt);

extern void init_queue(QueueType *q);
extern QueueStatus enqueue(QueueType *q, unsigned char str);
extern void SendString(USART_TypeDef* USARTx, const char* pstrmsg);
extern uint8_t Make_CheckSum(const char *Data_packet, int packet_size);

static void UART1_PutChar(uint8_t data);
extern void USART1_PutStr(const char *pStr);
static void USART2_PutChar(uint8_t data);
extern void USART2_PutStr(const char *pStr);

extern ErrorStatus usart1_getchar(char *data);
extern ErrorStatus usart2_getchar(char *data);

extern QueueType USART1_Queue;
extern QueueType USART2_Queue;

//extern uint16_t lvalue_parsing(uint8_t cnt, const char * rxbuf);
//extern uint8_t value_parsing(uint8_t cnt, const char * rxbuf);



//////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////



#endif