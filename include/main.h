///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef PANEL_MAIN_H_
#define PANEL_MAIN_H_
#define MCU_
#define STM32F_

//////////////////////////////////////////////////////////////////////////////////////////////////
// Include File
#include "global.h"
#include "stm32f10x_it.h"
#include "stm32f10x_init.h"
#include "math.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// Function Dec...
// User Define Function

///////////////////////////////////////////////////////////////////////////////////////////////////
// Interrupt Function
void SysTick_Handler(void);
void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
///////////////////////////////////////////////////////////////////////////////////////////////////

/* Exported functions ------------------------------------------------------- */

void Usart1_Parsing(void);
//void USART1_PutStr(char *pStr);
void Usart2_Parsing(void);
//void USART2_PutStr(char *pStr);

void TIM_Start(TIM_TypeDef* TIMx);
void TIM_Stop(TIM_TypeDef* TIMx);

static void com_processing(void);
static void init_value(void);


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////// COM  

static char Usart1_RXD;
static char Usart2_RXD;

QueueType USART1_Queue;
QueueType USART2_Queue;

static char RxBuffer1[256];
static char RxBuffer2[256];
static char RX_data=0;

static uint16_t RxCounter1=0;
static uint16_t RxCounter2=0;

static uint8_t usart1_headchk=0;
static uint8_t usart2_headchk=0;

static uint16_t temp_ore = 0;
//static char temp_ore = 0;

static uint8_t active=1;

//static uint16_t ADC1ConvertedValue[1];

//ADC
/////////////////////////////////////////////

static uint16_t ADC1ConValue_ch8=0;
static uint16_t readADC1_ch8(u8 ADC1_Channel_8);
static float chk_P3_3VIN=0;
static uint16_t temp_chk_P3_3VIN=0;

static uint16_t ADC1ConValue_ch9=0;
static uint16_t readADC1_ch9(u8 ADC1_Channel_9);
static float chk_P3_3VDCD=0;
static uint16_t temp_chk_P3_3VDCD=0;

static uint16_t ADC1ConValue_ch10=0;
static uint16_t readADC1_ch10(u8 ADC1_Channel_10);
static float chk_P3_3FPGA=0;
static uint16_t temp_chk_P3_3FPGA=0;

static uint16_t ADC1ConValue_ch11=0;
static uint16_t readADC1_ch11(u8 ADC1_Channel_11);
static float chk_P3_3STM=0;
static uint16_t temp_chk_P3_3STM=0;

static uint16_t ADC1ConValue_ch12=0;
static uint16_t readADC1_ch12(u8 ADC1_Channel_12);
static float chk_P5VIN=0;
static uint16_t temp_chk_P5VIN=0;

static uint16_t ADC1ConValue_ch13=0;
static uint16_t readADC1_ch13(u8 ADC1_Channel_13);
static float chk_P12VIN=0;
static uint16_t temp_chk_P12VIN=0;

static uint16_t ADC1ConValue_ch14=0;
static uint16_t readADC1_ch14(u8 ADC1_Channel_14);
static float chk_M5VIN=0;
static uint16_t temp_chk_M5VIN=0;

static uint16_t ADC1ConValue_ch15=0;
static uint16_t readADC1_ch15(u8 ADC1_Channel_15);
static float chk_M12VIN=0;
static uint16_t temp_chk_M12VIN=0;

typedef struct
{
  float BITE_P3_3VIN;
  float BITE_P3_3VDCD;
  float BITE_P3_3FPGA;
  float BITE_P3_3STM;
  float BITE_P5VIN;
  float BITE_P12VIN;
  float BITE_M5VIN;
  float BITE_M12VIN;
} REC_RWR_STATUS;

static REC_RWR_STATUS rec_pwr_status;


static bool trig_event=false;


#endif
